package pt.pedrovalerio.weatherapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import pt.pedrovalerio.weatherapp.api.model.WeatherData;
import pt.pedrovalerio.weatherapp.api.WeatherService;

/**
 * Created by Pedro on 08-12-2017.
 */

public class DashboardFragment extends Fragment implements Observer<WeatherData> {
    public static final String TAG = "DASHBOARD_TAG";
    private static final String WEATHER_DATA = "WEATHER_DATA";
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_info)
    ImageButton btnInfo;

    @BindView(R.id.btn_settings)
    ImageButton btnSettings;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    private WeatherData mWeatherData;
    private boolean temperatureInCelsius;
    private int numberOfDays;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, view);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(toolbar);
        appCompatActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);

        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayInfo();
            }
        });

        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSettings();
            }
        });

        SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        String location = Util.getSavedLocation(getContext(), sharedPreferences);
        temperatureInCelsius = Util.getCelsiusMode(getContext(), sharedPreferences);
        numberOfDays = Util.getNumberOfDays(getContext(), sharedPreferences);
        if(savedInstanceState != null && savedInstanceState.containsKey(WEATHER_DATA)){
            mWeatherData = (WeatherData) savedInstanceState.get(WEATHER_DATA);
            if(mWeatherData != null && location.equalsIgnoreCase(mWeatherData.getCityName()))
                displayWeatherData(mWeatherData);
            else {
                WeatherService.getForecast(location, "en",this);
            }
        }
        else {
            WeatherService.getForecast(location, "en",this);
        }


        return view;
    }

    private void openSettings() {
        NavigatorInterface navigatorInterface = (NavigatorInterface) getActivity();
        navigatorInterface.displaySettingsFragment();
    }

    private void displayInfo() {
        NavigatorInterface navigatorInterface = (NavigatorInterface) getActivity();
        navigatorInterface.displayInfoFragment();
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(WeatherData value) {
        mWeatherData = Util.arrangeData(value);
        displayWeatherData(mWeatherData);
    }

    private void displayWeatherData(WeatherData value) {
        recyclerView.setAdapter(new WeatherAdapter(getContext(), value, temperatureInCelsius, numberOfDays));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(WEATHER_DATA, mWeatherData);
        super.onSaveInstanceState(outState);

    }

}
