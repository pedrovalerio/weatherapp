package pt.pedrovalerio.weatherapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements NavigatorInterface {

    public static final java.lang.String PARENT_FRAGMENT = "parent_fragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState != null){
            // Restore fragments if there are fragments to restore
            int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
            Fragment fragment = null;
            String tag = null;
            if(backStackEntryCount > 0) {
                FragmentManager.BackStackEntry backStackEntry = getSupportFragmentManager().getBackStackEntryAt(backStackEntryCount - 1);
                if (backStackEntry.getName().equals(InfoFragment.TAG)) {
                    fragment = getSupportFragmentManager().findFragmentByTag(InfoFragment.TAG);
                    tag = InfoFragment.TAG;
                }
                if (backStackEntry.getName().equals(SettingsFragment.TAG)) {
                    fragment = getSupportFragmentManager().findFragmentByTag(SettingsFragment.TAG);
                    tag = SettingsFragment.TAG;
                }
            }
            else {
                fragment = getSupportFragmentManager().findFragmentByTag(DashboardFragment.TAG);
                tag = DashboardFragment.TAG;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, tag).commit();
        }
        else
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new DashboardFragment(), DashboardFragment.TAG).commit();
    }

    @Override
    public void displayInfoFragment() {
        Bundle bundle = new Bundle();
        bundle.putString(PARENT_FRAGMENT, getString(R.string.dashboard_title));
        InfoFragment infoFragment = new InfoFragment();
        infoFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, infoFragment, InfoFragment.TAG).addToBackStack(InfoFragment.TAG).commit();
    }

    @Override
    public void displaySettingsFragment() {
        Bundle bundle = new Bundle();
        bundle.putString(PARENT_FRAGMENT, getString(R.string.dashboard_title));
        SettingsFragment settingsFragment = new SettingsFragment();
        settingsFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, settingsFragment, SettingsFragment.TAG).addToBackStack(SettingsFragment.TAG).commit();
    }

}
