package pt.pedrovalerio.weatherapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Pedro on 10-12-2017.
 */

class PreferenceAdapter extends RecyclerView.Adapter<PreferenceAdapter.PrefViewHolder> {
    private SettingsListener mListener;
    private List<String> mOptions;
    private List<String> mKeys;
    private SharedPreferences mSharedPreferences;
    private Context mContext;

    public PreferenceAdapter(Context context, List<String> options, List<String> keys, SharedPreferences sharedPreferences, SettingsListener listener) {
        mContext = context;
        mOptions = options;
        mKeys = keys;
        mSharedPreferences = sharedPreferences;
        mListener = listener;
    }

    @Override
    public PrefViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.preferences_item, parent, false);
        PrefViewHolder viewHolder = new PrefViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PrefViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mOptions.size();
    }

    class PrefViewHolder extends RecyclerView.ViewHolder {
        View mView;

        public PrefViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void bind(int position) {
            TextView textViewTitle = mView.findViewById(R.id.pref_title);
            TextView textViewSummary = mView.findViewById(R.id.pref_summary);
            textViewTitle.setText(mOptions.get(position));
            String summary = null;
            switch (position) {
                case 0:
                    summary = Util.getSavedLocation(mContext, mSharedPreferences);
                    mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mListener.editLocation();
                        }
                    });
                    break;
                case 1:
                    boolean celsiusMode = Util.getCelsiusMode(mContext, mSharedPreferences);
                    summary = celsiusMode ? mContext.getString(R.string.unit_celsius) : mContext.getString(R.string.unit_fahrenheit);
                    mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mListener.editTemperatureUnits();
                        }
                    });
                    break;
                case 2:
                    int numberDays = Util.getNumberOfDays(mContext, mSharedPreferences);
                    summary = Integer.toString(numberDays);
                    mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mListener.openPicker();
                        }
                    });
                    break;
                default:
                    break;
            }
            textViewSummary.setText(summary);
        }
    }
}
