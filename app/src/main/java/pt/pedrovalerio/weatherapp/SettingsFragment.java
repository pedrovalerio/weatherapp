package pt.pedrovalerio.weatherapp;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Pedro on 10-12-2017.
 */

public class SettingsFragment extends Fragment implements SettingsListener, SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String TAG = "SETTINGS_TAG";
    @BindView(R.id.pref_list)
    RecyclerView mRecyclerView;

    @BindView(R.id.container)
    FrameLayout mContainer;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private static final int MIN_DAYS = 1;
    private static final int MAX_DAYS = 5;

    public SettingsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);

        initRecyclerView();

        View backButton = view.findViewById(R.id.btn_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        TextView parentFragment = toolbar.findViewById(R.id.parent_title);
        String parentTitle = getArguments().getString(MainActivity.PARENT_FRAGMENT, "");
        parentFragment.setText(parentTitle);
        TextView fragmentTitle = toolbar.findViewById(R.id.toolbar_title);
        fragmentTitle.setText(getText(R.string.settings_title));

        return view;
    }

    private void initRecyclerView() {
        List<String> options = Util.getPreferenceOptions(getContext());
        List<String> keys = Util.getPreferenceKeys(getContext());
        PreferenceAdapter preferenceAdapter = new PreferenceAdapter(getContext(), options, keys,
                getActivity().getPreferences(Context.MODE_PRIVATE), this);
        mRecyclerView.setAdapter(preferenceAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), linearLayoutManager.getOrientation()));
    }

    @Override
    public void editLocation() {
        dismissDialog();
        View view = LayoutInflater.from(getContext()).inflate(R.layout.location_edit, mContainer, false);
        Button cancelButton = view.findViewById(R.id.btn_cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null && imm.isAcceptingText())
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                dismissDialog();
            }
        });
        Button confirmButton = view.findViewById(R.id.btn_confirm);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editText = (EditText) mContainer.findViewById(R.id.location_edit_box);
                if (editText != null) {
                    String text = editText.getText().toString();
                    if (!text.isEmpty()) {
                        // Dismiss keyboard before removing the view
                        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm != null && imm.isAcceptingText())
                            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                        selectedNewLocation(text);
                    }
                }
            }
        });
        mContainer.addView(view);
        mContainer.setVisibility(View.VISIBLE);

    }


    private void dismissDialog() {
        mContainer.removeAllViews();
        mContainer.setVisibility(View.GONE);
    }


    @Override
    public void editTemperatureUnits() {
        dismissDialog();
        boolean tempInCelsius = Util.getCelsiusMode(getContext(), getActivity().getPreferences(Context.MODE_PRIVATE));
        View view = LayoutInflater.from(getContext()).inflate(R.layout.temperature_unit_picker, mContainer, false);
        TextView tvCelsius = view.findViewById(R.id.tv_celsius);
        tvCelsius.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectCelsius(true);
            }
        });
        final TextView tvFahrenheit = view.findViewById(R.id.tv_fahrenheit);
        tvFahrenheit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectCelsius(false);
            }

        });

        //TODO: Implement equivalent solution for API 23 and below.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (tempInCelsius) {
                tvCelsius.setTextAppearance(R.style.active_unit);
                tvFahrenheit.setTextAppearance(R.style.inactive_unit);
            } else {
                tvCelsius.setTextAppearance(R.style.inactive_unit);
                tvFahrenheit.setTextAppearance(R.style.active_unit);
            }

        } else {
            if (tempInCelsius) {
                tvCelsius.setTextColor(getContext().getResources().getColor(R.color.black));
                tvFahrenheit.setTextColor(getContext().getResources().getColor(R.color.grey_text));
            } else {
                tvCelsius.setTextColor(getContext().getResources().getColor(R.color.grey_text));
                tvFahrenheit.setTextColor(getContext().getResources().getColor(R.color.black));
            }
        }
        mContainer.addView(view);
        mContainer.setVisibility(View.VISIBLE);
        mContainer.invalidate();

    }


    @Override
    public void openPicker() {
        dismissDialog();
        View view = LayoutInflater.from(getContext()).inflate(R.layout.day_number_picker, mContainer, false);
        final NumberPicker numberPicker = view.findViewById(R.id.numberPicker);
        numberPicker.setMinValue(MIN_DAYS);
        numberPicker.setMaxValue(MAX_DAYS);
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldVal, int newValue) {
                selectedNumberOfDays(newValue);
            }
        });
        numberPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedNumberOfDays(numberPicker.getValue());
                dismissDialog();
            }
        });
        mContainer.addView(view);
        mContainer.setVisibility(View.VISIBLE);

    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        List<String> options = Util.getPreferenceOptions(getContext());
        List<String> keys = Util.getPreferenceKeys(getContext());
        PreferenceAdapter preferenceAdapter = new PreferenceAdapter(getContext(), options, keys,
                sharedPreferences, this);
        mRecyclerView.setAdapter(preferenceAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getPreferences(Context.MODE_PRIVATE).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().getPreferences(Context.MODE_PRIVATE).unregisterOnSharedPreferenceChangeListener(this);
    }


    private void selectedNewLocation(String text) {
        dismissDialog();
        Util.saveSharedPrefValue(getActivity().getPreferences(Context.MODE_PRIVATE),
                getContext().getString(R.string.prefs_location_key), StringUtils.capitalize(StringUtils.lowerCase(text)));
    }

    private void selectCelsius(boolean celsius) {
        dismissDialog();
        Util.setCelsiusMode(getContext(), getActivity().getPreferences(Context.MODE_PRIVATE), celsius);
    }

    private void selectedNumberOfDays(int days) {
        Util.setNumberOfDays(getContext(), getActivity().getPreferences(Context.MODE_PRIVATE), days);
    }
}
