package pt.pedrovalerio.weatherapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Pedro on 08-12-2017.
 */

public class InfoFragment extends Fragment {
    public static final String TAG = "INFO_TAG";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        View backButton = view.findViewById(R.id.btn_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        TextView parentFragment = toolbar.findViewById(R.id.parent_title);
        String parentTitle = getArguments().getString(MainActivity.PARENT_FRAGMENT, "");
        parentFragment.setText(parentTitle);
        TextView fragmentTitle = toolbar.findViewById(R.id.toolbar_title);
        fragmentTitle.setText(getText(R.string.info_title));

        return view;
    }
}
