package pt.pedrovalerio.weatherapp.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import pt.pedrovalerio.weatherapp.api.model.WeatherData;
import pt.pedrovalerio.weatherapp.api.model.WeatherForecast;

/**
 * Created by Pedro on 08-12-2017.
 */

class WeatherDataConverter implements JsonDeserializer<WeatherData>{
    @Override
    public WeatherData deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        WeatherData weatherData = new WeatherData();
        JsonObject cityObject = json.getAsJsonObject().getAsJsonObject("city");
        if(cityObject != null && !cityObject.isJsonNull()){
            String cityName = cityObject.get("name").getAsString();
            weatherData.setCityName(cityName);
        }
        List<WeatherForecast> forecastList = new ArrayList<>();
        JsonArray forecastJsonArray = json.getAsJsonObject().getAsJsonArray("list");
        if(forecastJsonArray != null && !forecastJsonArray.isJsonNull()){
            for(int i = 0; i < forecastJsonArray.size(); i ++){
                JsonObject forecastJson = forecastJsonArray.get(i).getAsJsonObject();
                long timestamp = forecastJson.get("dt").getAsLong();
                float temp = forecastJson.getAsJsonObject("main").get("temp").getAsFloat();
                float minTemp = forecastJson.getAsJsonObject("main").get("temp_min").getAsFloat();
                float maxTemp = forecastJson.getAsJsonObject("main").get("temp_max").getAsFloat();

                // Currently OWM API returns weather as a list containing a single Object
                JsonObject weatherObject = forecastJson.getAsJsonArray("weather").get(0).getAsJsonObject();
                int weatherId = weatherObject.get("id").getAsInt();
                WeatherForecast weatherForecast = new WeatherForecast(timestamp, temp, minTemp, maxTemp, weatherId );
                forecastList.add(weatherForecast);
            }
        }
        weatherData.setList(forecastList);
        return weatherData;
    }
}
