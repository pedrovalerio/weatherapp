package pt.pedrovalerio.weatherapp.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import pt.pedrovalerio.weatherapp.api.model.WeatherData;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Pedro on 08-12-2017.
 */

public class WeatherService {
    //TODO: Bad practice to keep API_KEY on public repository
    // Usually I'd move this API key off to gradle.properties, add a buildConfigField and access it through BuildConfig
    private static final String API_KEY = "af16af073cd19d5ccb8a7c6ca0da7842";
    private static final String API_BASE_URL = "http://api.openweathermap.org/data/2.5/";

    public static void getForecast(String city, String lang, Observer<WeatherData> observer){
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(WeatherData.class, new WeatherDataConverter())
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        WeatherServiceInterface weatherServiceInterface = retrofit.create(WeatherServiceInterface.class);
        Observable<WeatherData> weatherDataObservable = weatherServiceInterface.getWeatherForCity(city, lang, API_KEY);

        weatherDataObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }
}
