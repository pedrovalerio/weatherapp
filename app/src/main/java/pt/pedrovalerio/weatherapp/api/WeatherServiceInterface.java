package pt.pedrovalerio.weatherapp.api;


import io.reactivex.Observable;
import pt.pedrovalerio.weatherapp.api.model.WeatherData;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Pedro on 08-12-2017.
 */

public interface WeatherServiceInterface {
    @GET("forecast?units=metric")
    Observable<WeatherData> getWeatherForCity(@Query("q") String city,
                                              @Query("lang") String lang,
                                              @Query("appid") String apiKey);
}
