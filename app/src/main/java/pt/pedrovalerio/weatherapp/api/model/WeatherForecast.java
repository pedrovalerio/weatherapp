package pt.pedrovalerio.weatherapp.api.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Pedro on 08-12-2017.
 */

public class WeatherForecast implements Serializable {
    long timestamp;
    float currentTemp;
    float minTemp;
    float maxTemp;
    int weatherId;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public float getCurrentTemp() {
        return currentTemp;
    }

    public void setCurrentTemp(float currentTemp) {
        this.currentTemp = currentTemp;
    }

    public float getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(float minTemp) {
        this.minTemp = minTemp;
    }

    public float getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(float maxTemp) {
        this.maxTemp = maxTemp;
    }

    public int getWeatherId() {
        return weatherId;
    }

    public void setWeatherId(int weatherId) {
        this.weatherId = weatherId;
    }

    public WeatherForecast() {

    }

    public WeatherForecast(long timestamp, float currentTemp, float minTemp, float maxTemp, int weatherId) {
        this.timestamp = timestamp;
        this.currentTemp = currentTemp;
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
        this.weatherId = weatherId;
    }
}
