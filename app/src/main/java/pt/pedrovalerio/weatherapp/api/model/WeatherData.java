package pt.pedrovalerio.weatherapp.api.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Pedro on 08-12-2017.
 */

public class WeatherData implements Serializable {
    List<WeatherForecast> forecasts;
    String cityName;

    public String getCityName() {
        return cityName;
    }

    public List<WeatherForecast> getForecasts() {
        return forecasts;
    }

    public void setList(List<WeatherForecast> list) {
        this.forecasts = list;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
