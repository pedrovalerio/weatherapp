package pt.pedrovalerio.weatherapp;

/**
 * Created by Pedro on 08-12-2017.
 */

interface NavigatorInterface {
    void displayInfoFragment();
    void displaySettingsFragment();
}
