package pt.pedrovalerio.weatherapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;

import pt.pedrovalerio.weatherapp.api.model.WeatherData;
import pt.pedrovalerio.weatherapp.api.model.WeatherForecast;

/**
 * Created by Pedro on 09-12-2017.
 */

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder> {
    private static final int FIRST_POSITION = 1;
    private static final int OTHER_POSITIONS = 2;
    private final int mNumberItems;
    private WeatherData mWeatherData;
    private Context mContext;
    private Calendar mCalendar;
    private boolean celsiusMode;

    public WeatherAdapter(Context context, WeatherData weatherData, boolean inCelsius, int num){
        mContext = context;
        mWeatherData = weatherData;
        mCalendar = Calendar.getInstance();
        celsiusMode = inCelsius;
        mNumberItems = num;
    }

    @Override
    public WeatherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout;
        if(viewType == FIRST_POSITION)
            layout = R.layout.list_header;
        else
            layout = R.layout.list_item;
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new WeatherViewHolder(view);
    }

    @Override
    public void onBindViewHolder(WeatherViewHolder holder, int position) {
        holder.bindData(mWeatherData.getForecasts().get(position), position);
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0)
            return FIRST_POSITION;
        return OTHER_POSITIONS;
    }

    @Override
    public int getItemCount() {
        return Math.min(mWeatherData.getForecasts().size(), mNumberItems);
    }

    public class WeatherViewHolder extends RecyclerView.ViewHolder {
        View view;


        public WeatherViewHolder(View itemView) {
            super(itemView);
            view = itemView;
        }

        public void bindData(WeatherForecast weatherForecast, int position){
            TextView dayView = view.findViewById(R.id.day);
            ImageView icon = view.findViewById(R.id.weather_icon);
            TextView lowTemperature =  view.findViewById(R.id.low_temperature);
            TextView highTemperature = view.findViewById(R.id.high_temperature);
            String dayOfWeek;

            // Timestamp from OWM is in seconds since epoch, need to convert to milliseconds since epoch
            mCalendar.setTimeInMillis(weatherForecast.getTimestamp()*1000L);
            if(celsiusMode) {
                lowTemperature.setText(String.format("%d", (int) weatherForecast.getMinTemp()));
                highTemperature.setText(String.format("%d", (int) weatherForecast.getMaxTemp()));
            }
            else{
                lowTemperature.setText(String.format("%d", (int) Util.celsiusToFahrenheit(weatherForecast.getMinTemp())));
                highTemperature.setText(String.format("%d", (int) Util.celsiusToFahrenheit(weatherForecast.getMaxTemp())));
            }
            int drawableId = Util.getDrawableForWeatherId(weatherForecast.getWeatherId());
            icon.setImageDrawable(mContext.getResources().getDrawable(drawableId));
            String weatherDescription = mContext.getString(Util.getDescriptionForWeatherId(weatherForecast.getWeatherId()));
            icon.setContentDescription(weatherDescription);
            switch(position){
                case 0:
                    TextView currentTemperature =  view.findViewById(R.id.current_temperature);
                    Float temperature = weatherForecast.getCurrentTemp();
                    if(!celsiusMode)
                        temperature = Util.celsiusToFahrenheit(temperature);
                    currentTemperature.setText(Util.getFormattedTemperature(temperature));

                    TextView city =  view.findViewById(R.id.city_name);
                    city.setText(mWeatherData.getCityName());
                    TextView weatherDesc =  view.findViewById(R.id.current_weather);
                    weatherDesc.setText(weatherDescription);
                    dayOfWeek = Util.getFormattedDayString(mCalendar.getTime(), mContext.getString(R.string.string_today));
                    dayView.setText(dayOfWeek);
                    break;
                case 1:
                    dayOfWeek = mContext.getString(R.string.string_tomorrow);
                    dayView.setText(dayOfWeek);
                    break;
                default:
                    dayOfWeek = mContext.getString(Util.getDayOfWeekString(mCalendar.get(Calendar.DAY_OF_WEEK)));
                    dayView.setText(dayOfWeek);

            }

        }
    }
}
