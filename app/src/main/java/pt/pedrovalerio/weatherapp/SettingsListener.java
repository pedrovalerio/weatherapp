package pt.pedrovalerio.weatherapp;

/**
 * Created by Pedro on 11-12-2017.
 */

interface SettingsListener {
    void editLocation();
    void editTemperatureUnits();
    void openPicker();

}
