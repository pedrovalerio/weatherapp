package pt.pedrovalerio.weatherapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import pt.pedrovalerio.weatherapp.api.model.WeatherData;
import pt.pedrovalerio.weatherapp.api.model.WeatherForecast;

/**
 * Created by Pedro on 10-12-2017.
 */

public class Util {

    public static WeatherData arrangeData(WeatherData weatherData){
        WeatherData newWeatherData = new WeatherData();
        newWeatherData.setCityName(weatherData.getCityName());
        Calendar calendar = Calendar.getInstance();
        List<WeatherForecast> weatherForecasts = new ArrayList<>();
        int i = 0;
        WeatherForecast weatherForecast = new WeatherForecast();
        weatherForecast.setTimestamp(weatherData.getForecasts().get(0).getTimestamp());
        weatherForecast.setCurrentTemp(weatherData.getForecasts().get(0).getCurrentTemp());
        float minTemp = weatherData.getForecasts().get(0).getMinTemp();
        float maxTemp = weatherData.getForecasts().get(0).getMaxTemp();
        int weatherId = 1000;
        calendar.setTimeInMillis(weatherForecast.getTimestamp() * 1000L);
        while(i < weatherData.getForecasts().size()){
            WeatherForecast weatherForecast2 = weatherData.getForecasts().get(i);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTimeInMillis(weatherForecast2.getTimestamp() * 1000L);
            if(calendar.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR)){
                minTemp = Math.min(minTemp, weatherForecast2.getMinTemp());
                maxTemp = Math.max(maxTemp, weatherForecast2.getMaxTemp());
                weatherId = Math.min(weatherId, weatherForecast2.getWeatherId());
            }
            else {
                weatherForecast.setMinTemp(minTemp);
                weatherForecast.setMaxTemp(maxTemp);
                weatherForecast.setWeatherId(weatherId);
                weatherForecasts.add(weatherForecast);
                Log.i("UTIL DEBUG", "DAY: " + calendar2.getTime().toString());
                weatherForecast = new WeatherForecast();
                weatherForecast.setTimestamp(weatherForecast2.getTimestamp());
                calendar.setTimeInMillis(weatherForecast2.getTimestamp()*1000L);
                minTemp = weatherForecast2.getMinTemp();
                maxTemp = weatherForecast2.getMaxTemp();
                weatherId = 1000;
            }
            i++;
        }
        weatherForecast.setMinTemp(minTemp);
        weatherForecast.setMaxTemp(maxTemp);
        weatherForecast.setWeatherId(weatherId);
        weatherForecasts.add(weatherForecast);

        newWeatherData.setList(weatherForecasts);
        return newWeatherData;
    }

    public static int getDayOfWeekString(int index){
        switch(index){
            case Calendar.MONDAY:
                return R.string.string_monday;
            case Calendar.TUESDAY:
                return R.string.string_tuesday;
            case Calendar.WEDNESDAY:
                return R.string.string_wednesday;
            case Calendar.THURSDAY:
                return R.string.string_thursday;
            case Calendar.FRIDAY:
                return R.string.string_friday;
            case Calendar.SATURDAY:
                return R.string.string_saturday;
            default:
                return R.string.string_sunday;
        }

    }

    public static WeatherData getMockWeatherData(){
        WeatherData weatherData = new WeatherData();
        List<WeatherForecast> forecastList = new ArrayList();
        forecastList.add(new WeatherForecast(1512864000L, 14.42f, 12.73f, 14.42f,
                800));
        forecastList.add(new WeatherForecast(1512950400L, 8.22f, 8.22f, 10.56f,
                212));
        forecastList.add(new WeatherForecast(1513036800L, 4.11f, 4.11f, 4.12f,
                804));
        forecastList.add(new WeatherForecast(1513123200L, 14.42f, 10.31f, 10.31f,
                615));
        forecastList.add(new WeatherForecast(1513209600L, 14.42f, 9.21f, 9.21f,
                504));
        forecastList.add(new WeatherForecast(1513285200L, 14.42f, 12.73f, 14.42f,
                800));
        forecastList.add(new WeatherForecast(1513285200L, 14.42f, 12.73f, 14.42f,
                800));
        weatherData.setCityName("Rotterdam");
        weatherData.setList(forecastList);

        return weatherData;
    }

    public static int getDescriptionForWeatherId(int weatherId) {
        if(weatherId > 800 && weatherId < 900)
            return R.string.weather_id_8xx;
        if(weatherId == 800)
            return R.string.weather_id_800;
        if(weatherId >= 600)
            return R.string.weather_id_6xx;
        if(weatherId >= 500)
            return R.string.weather_id_5xx;
        if(weatherId >= 300)
            return R.string.weather_id_3xx;
        if(weatherId >= 200)
            return R.string.weather_id_2xx;
        //TODO: Create drawable (error or n/a ) for when the weatherId falls out of these ranges
        return 0;
    }

    public static int getDrawableForWeatherId(int weatherId) {
        if(weatherId > 800 && weatherId < 900)
            return R.drawable.ic_clouds;
        if(weatherId == 800)
            return R.drawable.ic_sun;
        if(weatherId >= 600)
            return R.drawable.ic_snow;
        if(weatherId >= 500)
            return R.drawable.ic_rain;
        if(weatherId >= 300)
            return R.drawable.ic_rain;
        if(weatherId >= 200)
            return R.drawable.ic_storm;
        return 0;
    }

    public static String getFormattedDayString(Date date, String dayOfWeek){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE d MMM", Locale.getDefault());
        String formattedDayString = String.format("%s\t%s", dayOfWeek,
                simpleDateFormat.format(date) );
        return StringUtils.capitalize(formattedDayString);
    }

    public static String getFormattedTemperature(float currentTemp) {
        return String.format("%dº", (int) currentTemp);
    }

    public static List<String> getPreferenceOptions(Context context){
        String[] options = context.getResources().getStringArray(R.array.preference_options);
        return Arrays.asList(options);
    }

    public static List<String> getPreferenceKeys(Context context){
        String[] keys = context.getResources().getStringArray(R.array.preference_keys);
        return Arrays.asList(keys);
    }

    public static void saveSharedPrefValue(SharedPreferences preferences, String string, String text) {
        preferences.edit().putString(string, text).apply();
    }

    public static String getSavedLocation(Context context, SharedPreferences preferences){
        return preferences.getString(context.getString(R.string.prefs_location_key), context.getString(R.string.prefs_location_default) );
    }

    public static boolean getCelsiusMode(Context context, SharedPreferences preferences){
        return preferences.getBoolean(context.getString(R.string.prefs_units_key), context.getResources().getBoolean(R.bool.celsiusMode) );
    }

    public static void setCelsiusMode(Context context, SharedPreferences preferences, boolean celsius){
        preferences.edit().putBoolean(context.getString(R.string.prefs_units_key), celsius).apply();
    }

    public static void setNumberOfDays(Context context, SharedPreferences preferences, int number){
        preferences.edit().putInt(context.getString(R.string.prefs_picker_key), number).apply();
    }

    public static int getNumberOfDays(Context context, SharedPreferences preferences){
        return preferences.getInt(context.getString(R.string.prefs_picker_key), context.getResources().getInteger(R.integer.prefs_picker_number_default));
    }

    public static float celsiusToFahrenheit(Float celsius){
        float fahrenheit = ( (9.0f/5.0f) * celsius) + 32;
        return fahrenheit;
    }
}
